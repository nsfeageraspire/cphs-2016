# README #

LaTeX source for a paper to be submitted to conferences.

Submit to [IROS](http://iros2017.org) by March 1.

Up to 6 pages and 6 MB.  

Optional video up to 10 MB.

[Call for papers](http://iros2017.org/images/CallForPapers_IROS2017.pdf): "broad theme of building friendly robots."

[Paper format](http://ras.papercept.net/conferences/support/support.php).
[LaTeX template](http://ras.papercept.net/conferences/support/tex.php).
Submit via [PaperPlaza](https://ras.papercept.net/conferences/scripts/start.pl).
